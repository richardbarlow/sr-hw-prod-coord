Kits are to be shipped from the KPE venue to UK Postbox, who will then manage shipping to Kickstart locations.

54 kits will be packed. Rich will take 6 back to Oxford with him (we had 5 teams at the Oxford Kickstart last year). That leaves 48 kits to ship to UK Postbox.

We will ship the kits on pallets. A standard pallet is 1.2x1.0m. An 18l RUB is 480x390x200mm.

Each pallet can have 6 (2x3) RUBs on each layer. Generally <2m tall is acceptable as a 'full' pallet. That means 8 layers at 0.2m each. 1.6m of load and ~0.2m of pallet. 1.8m total height.

Needs 50m of strapping and ~300m of stretch wrap.
