SR2017 Kit Packing Event - Post Event Review
============================================

Summary
-------

The SR2017 Kit Packing Event took place on the 9th, 10th and 11th September
2016. During this event 54 kits were fully packed and sealed. 48 of them were
shipped to UK Postbox on a pallet and the remaining 6 were taken back to
Oxford by Rich Barlow. The reason for taking 6 to Oxford was twofold: it
potentially reduced the postage costs for shipping kits to the Oxford
Kickstart and it aided in shipping German kits, where the batteries had to be
removed and managed separately.

The event began with the collated kit produced at the end of the preceding
Kit Collation Event. On the 9th the boxes containing the collated kit were
transported from the storage container in Newbury to the packing venue in
Basingstoke. The morning of the 10th was spent preparing wire and
camcons and laying out all of the kit ready for filling into boxes. The
afternoon of the 10th was spent filling boxes and updating (scanning) the
location of their contents in the Inventory. The majority of the 11th was
spent checking and packaging the contents of the boxes. The remainder of the
11th was spent sealing the boxes, updating the Inventory and strapping them
to a pallet.

During the event it became clear that the process could be optimised. The
process took approximately 35 person-hours. This can potentially be reduced
to 20 person-hours. This will be discussed in detail later on.

Activities on the 9th
---------------------

The boxes containing the collated kit were transported from the storage
container in Newbury to the packing venue in Basingstoke. This took
approximately 1 hour (from 12:30 to 13:30). However, this task in general took
much more than the single hour of transporting due to the time taken to hire
the van. It also didn't include moving the boxes into the room used for
packing over the next two days. Had the boxes already been physically near to
the room used to perform the packing, this day would not have been necessary.

This day took approximately 3 person-hours.

Activities on the 10th
----------------------

The day began at 9:10 and ended at 16:40.

The first task was to unload the boxes from the van into the packing room.
This took until 10:10 (1 hour). Two people performed this task, taking 2
person hours.

The morning consisted of three simultaneous tasks being carried out:

 * Wire was cut to length and knotted into bundles. 10:30 to 13:00 (3 hours).
   On average 2.5 people performed this task totalling 7.5 person-hours.
 * Camcons were counted out into bags. 11:00 to 12:45 (1 hour 45 minutes).
   One person performed this task.
 * Other parts of the kit were laid out onto desks ready for them to be placed
   into kit boxes. 10:45 to 12:45 (2 hours).
   One person performed this task.

The afternoon consisted of simultaneously filling kit boxes and scanning their
contents to update the state recorded in the Inventory. The filling began at
14:30 and ended at 15:45 (1 hour 15 minutes) and the scanning began at 14:30
and ended at 16:00 (1 hour 30 minutes). Two people filled the boxes, taking
2.5 person-hours and two people scanned the boxes, taking 3 person hours.
After the filling and scanning a further 40 minutes was spent moving piles of
boxes around and sorting the remaining parts; there were development parts and
spare non-development parts left that had to be sorted into boxes. There were
two people doing this, taking approximately 1.5 person-hours.

In general all tasks on this day went exactly as planned. The `WIRE`, `CAMCON`
and `LAYOUT` tasks were all carried out in parallel and the `FILL` and `SCAN`
tasks occurred simultaneously, with each kit flowing through the two stages
sequentially.

In total the tasks carried out on the 10th took just over 20 person-hours.

Activities on the 11th
----------------------

The day began at 9:45 and ended at 19:45.

The tasks remaining were to check and package the contents of each kit. In the
plan these are split into three distinct stages of `CHECK`, `JIFFY` and `PACK`.
Due to the limited number of people available we decided to run each kit
through this sequence of operations fully, rather than run all of the kits
through each stage fully before moving onto the next stage. This increased the
throughput greatly.

The checking and packing process began at 9:45 and ended at 15:15. This
includes a 1 hour break for lunch. Therefore it took 4 hours 30 minutes. Two
people performed these tasks, taking 9 person-hours. Due to the way in which
these tasks were performed, it is difficult to give a per-task breakdown of
time spent. However, it was roughly 3.5 person-hours for checking, 3.5
person-hours for jiffy bagging and 2 person-hours for packing into boxes.

Once all of the kits were packed tape was applied over the box handles, which
took approximately 30 minutes for two people (1 person-hour). This was not
originally planned for and should have been part of the `PACK` stage.

All of the packed boxes were scanned and updated in the Inventory. This took
approximately 15 minutes for two people (0.5 person-hours) and again, was not
originally planned for and should have been part of the `PACK` stage.

At 17:00 the kits were transported to the pallet loading bay. This took
approximately 30 minutes (two people, 1 person-hour).

The pallet loading began at 17:30 and ended at 18:30 (1 hour). Two people did
this, taking 2 person-hours.

The remainder of the day was spent tidying up the packing room. This took
approximately 1 person-hour.

In total the tasks carried out on the 11th took 14.5 person-hours.
