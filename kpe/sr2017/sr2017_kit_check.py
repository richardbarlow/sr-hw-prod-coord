#!/usr/bin/env python
from itertools import groupby
import os
from sys import argv
import sr.inventory.query as query

kit_dir = os.path.abspath(argv[1])

class PartStats(object):
    repr_header = "Part                                     Total  Packing Loc Total(diff)  Packing Loc Not Broken\n"\
                  "-----------------------------------------------------------------------------------------------\n"
    def __init__(self, inv, part_type, parts_per_kit):
        self.inv = inv
        self.part_type = part_type
        self.all_parts = self._query()
        self.loc_parts = [x for x in self.all_parts if kit_dir in x.path]
        self.total = len(self.all_parts)
        self.packing_location_total = len(self.loc_parts)
        self.packing_location_working = len([x for x in self.loc_parts if self._is_ok(x)])
        self.parts_per_kit = parts_per_kit

    def _is_ok(self, inv_part):
        if hasattr(inv_part, "elements"):
            return True
        else:
            return inv_part.condition == "working" and inv_part.info["physical_condition"] == "working"

    def _query(self, extra=""):
            return query.query("type is {0} not development is true {1}".format(self.part_type, extra), inv=self.inv)

    def __repr__(self):
        diff = self.packing_location_total - (self.parts_per_kit * 54)
        return "{0:40} {1:3d}   {2:3d}({3:+3d})                {4:3d}".format(
            self.part_type, self.total, self.packing_location_total, diff, self.packing_location_working
        )

class KitStats(object):
    kit_parts = [
        ('battery-lipo-11.1v2.2-turnigy', 2),
        ('lipo-bag*', 1),
        ('battery-charger-imax-b6', 1),
        ('battery-charger-e4', 1),
        ('battery-charger-*-supply', 1),
        ('odroid-u3', 1),
        ('motor-board-mcv4b', 2),
        ('power-board-pbv4b', 1),
        ('ruggeduino', 1),
        ('screw-shield-usb-side', 1),
        ('screw-shield-pwr-side', 1),
        ('servo-board-sbv4b', 1),
        ('usb-hub-startech', 2),
        ('usb-wifi-tl-wn823n', 2),
        ('tablet-rc0734h', 1),
        ('usb-charger', 1),
        ('webcam-logitech*', 1),
        ('box-18l-rub', 1),
    ]

    def __init__(self):
        self.inv = query._get_inv()
        self.part_stats = [PartStats(self.inv, part[0], part[1]) for part in self.kit_parts]

    def __repr__(self):
        return PartStats.repr_header + "\n".join(str(p) for p in self.part_stats)

def foo(path):
    expected_parts_set = set(KitStats.kit_parts)

    actual_parts = sorted(query.query("path is broomcupboard/packed_kits/{0}".format(path)), key=lambda x: x.name)
    actual_parts_set = set((part[0], len(list(part[1]))) for part in groupby(actual_parts, key=lambda y: y.name))
    missing_parts = expected_parts_set - actual_parts_set
    extra_parts = actual_parts_set - expected_parts_set

    print "Missing: "
    print missing_parts
    print "Extra: "
    print extra_parts


kit_stats = KitStats()
print kit_stats
#foo(argv[1])
